package meetingroom.ui.login;

import javax.swing.JFrame;
import meetingroom.ui.reservasi.PengajuanRuanganScreen;
import meetingroom.ui.reservasi.PersetujuanRuanganScreen;
import meetingroom.ui.master.PaketScreen;
import meetingroom.ui.master.ParameterScreen;
import meetingroom.ui.master.RuanganScreen;
import meetingroom.ui.master.UserScreen;
import meetingroom.ui.report.ReportRuanganScreen;

public class HomeScreen extends javax.swing.JFrame {

    public HomeScreen() {
        initComponents();
        setExtendedState(JFrame.MAXIMIZED_HORIZ);
        labelWelcome.setText("Selamat Datang Lukman Arizal");
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        labelWelcome = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        home = new javax.swing.JMenu();
        menuExit = new javax.swing.JMenuItem();
        menuReservasi = new javax.swing.JMenu();
        menuPengajuanReservasi = new javax.swing.JMenuItem();
        menuApprovalReservasi = new javax.swing.JMenuItem();
        menuUtility = new javax.swing.JMenu();
        menuRuangan = new javax.swing.JMenuItem();
        menuPaket = new javax.swing.JMenuItem();
        menuUser = new javax.swing.JMenuItem();
        menuParameter = new javax.swing.JMenuItem();
        menuReport = new javax.swing.JMenu();
        menuDaftarReservasi = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Segoe UI Light", 1, 36)); // NOI18N
        jLabel1.setText("HOME SCREEN");

        labelWelcome.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N

        home.setText("Home");

        menuExit.setText("Exit");
        menuExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuExitActionPerformed(evt);
            }
        });
        home.add(menuExit);

        jMenuBar1.add(home);

        menuReservasi.setText("Reservasi");

        menuPengajuanReservasi.setText("Pengajuan Reservasi");
        menuPengajuanReservasi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPengajuanReservasiActionPerformed(evt);
            }
        });
        menuReservasi.add(menuPengajuanReservasi);

        menuApprovalReservasi.setText("Approval Reservasi");
        menuApprovalReservasi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuApprovalReservasiActionPerformed(evt);
            }
        });
        menuReservasi.add(menuApprovalReservasi);

        jMenuBar1.add(menuReservasi);

        menuUtility.setText("Utility");

        menuRuangan.setText("Ruangan");
        menuRuangan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuRuanganActionPerformed(evt);
            }
        });
        menuUtility.add(menuRuangan);

        menuPaket.setText("Paket");
        menuPaket.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPaketActionPerformed(evt);
            }
        });
        menuUtility.add(menuPaket);

        menuUser.setText("User");
        menuUser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuUserActionPerformed(evt);
            }
        });
        menuUtility.add(menuUser);

        menuParameter.setText("Parameter");
        menuParameter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuParameterActionPerformed(evt);
            }
        });
        menuUtility.add(menuParameter);

        jMenuBar1.add(menuUtility);

        menuReport.setText("Report");

        menuDaftarReservasi.setText("Daftar Reservasi");
        menuDaftarReservasi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDaftarReservasiActionPerformed(evt);
            }
        });
        menuReport.add(menuDaftarReservasi);

        jMenuBar1.add(menuReport);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(labelWelcome, javax.swing.GroupLayout.PREFERRED_SIZE, 903, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(18, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelWelcome, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(401, Short.MAX_VALUE))
        );

        setBounds(600, 200, 947, 562);
    }// </editor-fold>//GEN-END:initComponents

    private void menuRuanganActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuRuanganActionPerformed
        new RuanganScreen().setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_menuRuanganActionPerformed

    private void menuPaketActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPaketActionPerformed
        new PaketScreen().setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_menuPaketActionPerformed

    private void menuUserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuUserActionPerformed
        new UserScreen().setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_menuUserActionPerformed

    private void menuParameterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuParameterActionPerformed
        new ParameterScreen().setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_menuParameterActionPerformed

    private void menuExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuExitActionPerformed
        new LoginScreen().setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_menuExitActionPerformed

    private void menuPengajuanReservasiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPengajuanReservasiActionPerformed
        PengajuanRuanganScreen pengajuan = new PengajuanRuanganScreen();
        pengajuan.setVisible(true);
        pengajuan.setUsername(user);
        this.setVisible(false);
    }//GEN-LAST:event_menuPengajuanReservasiActionPerformed

    private void menuApprovalReservasiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuApprovalReservasiActionPerformed
        PersetujuanRuanganScreen persetujuan = new PersetujuanRuanganScreen();
        persetujuan.setVisible(true);
        persetujuan.setUsername(user);
        this.setVisible(false);
    }//GEN-LAST:event_menuApprovalReservasiActionPerformed

    private void menuDaftarReservasiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDaftarReservasiActionPerformed
        new ReportRuanganScreen().setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_menuDaftarReservasiActionPerformed

    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(HomeScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new HomeScreen().setVisible(true);
            }
        });
    }
    
    String user;
    
    public String setUsername(String username) {
        this.user = username;
        return username;
    }
    
    public String getUsername() {
        return this.user;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu home;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JLabel labelWelcome;
    private javax.swing.JMenuItem menuApprovalReservasi;
    private javax.swing.JMenuItem menuDaftarReservasi;
    private javax.swing.JMenuItem menuExit;
    private javax.swing.JMenuItem menuPaket;
    private javax.swing.JMenuItem menuParameter;
    private javax.swing.JMenuItem menuPengajuanReservasi;
    private javax.swing.JMenu menuReport;
    private javax.swing.JMenu menuReservasi;
    private javax.swing.JMenuItem menuRuangan;
    private javax.swing.JMenuItem menuUser;
    private javax.swing.JMenu menuUtility;
    // End of variables declaration//GEN-END:variables

}
