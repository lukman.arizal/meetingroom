/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package meetingroom.util;

import com.toedter.calendar.JDateChooser;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Legion
 */
public class MeetingHelper {
    
    public static boolean validateTextRequired(JTextField fldScreen) {
        if (fldScreen.getText().equals("")) {
            fldScreen.requestFocus();
            JOptionPane.showMessageDialog(null, "Data Harus diisi");
            return false;
        } else {
            return true;
        }
    }
    
    public static boolean validateComboboxRequired(JComboBox fldScreen) {
        if (fldScreen.getSelectedIndex() == '0') {
            fldScreen.requestFocus();
            JOptionPane.showMessageDialog(null, "Data Harus diisi");
            return false;
        } else {
            return true;
        }
    }
    
    public static boolean validateCalendarRequired(JDateChooser fldScreen) {
        if (fldScreen.getDate() == null) {
            fldScreen.requestFocus();
            JOptionPane.showMessageDialog(null, "Data Harus diisi");
            return false;
        } else {
            return true;
        }
    }
}
